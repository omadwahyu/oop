<?php
	require_once("animal.php");
	require_once("ape.php");
	require_once("frog.php");
	$animal = new Animal("shaun");
	echo "Name : ".$animal->name."<br>";
	echo "legs : ".$animal->legs."<br>";
	echo "cold blooded :".$animal->cold_blooded."<br><br>";

	$kodok = new Frog("Buduk");
	echo "Name : ".$kodok->name."<br>";
	echo "legs : ".$kodok->legs."<br>";
	echo "cold blooded : ".$kodok->cold_blooded."<br>";
	echo $kodok->jump()."<br><br>";

	$kera = new Ape("Kera Sakti");
	echo "Name : ".$kera->name."<br>";
	echo "legs : ".$kera->legs."<br>";
	echo "cold blooded : ".$kera->cold_blooded."<br>";
	echo $kera->yell()."<br><br>";

	

?>